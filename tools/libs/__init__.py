
class definition:
    def __init__(self, name):
        self.name = name
        self.pairs = []

    def out(self, f):
        print(self.name, file=f)
        strings = map(lambda p: "  " + p[0] + " " + p[1], self.pairs)
        f.write(" ;\n".join(strings))
        print(" .", file=f)
        print('', file=f)

    def p(self, x, y):
        self.pairs.append((x, y))

