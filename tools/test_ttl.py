#!/usr/bin/env python3

import rdflib
from sys import argv

g = rdflib.Graph()
g.load(argv[1], format='turtle')
