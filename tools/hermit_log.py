#!/usr/bin/env python3

from sys import argv

log = open(argv[1], 'r')
assert log.readline().strip() == "Classes equivalent to 'owl:Nothing':"
assert log.readline().strip() == "owl:Nothing"
assert not log.readline().startswith("\t")
