#!/usr/bin/env python3

import rdflib
from sys import argv

g = rdflib.Graph()
g.load(argv[1], format='turtle')

g.serialize(destination=argv[2], format='xml')
