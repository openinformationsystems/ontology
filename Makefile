objects = product.ttlo cuisine.ttlo recipe.ttlo \
		  recipe_category.ttlo recipe_ingredient.ttlo \
		  recipe_step.ttlo product_nutrition.ttlo \
		  product_shop.ttlo product_in_shop.ttlo

all: fff.ttl

fff.ttl: preamble.ttl $(objects)
	cat preamble.ttl *.ttlo > fff.ttl

fff.xml: fff.ttl
	tools/to_xml.py $< $@

fff.owl: fff.xml
	java -jar tools/ontology-convert.jar 'http://food.rubdos.be/' file://$(PWD)/fff.ttl

%.ttlo: %.yaml tools/turtlify.py
	tools/turtlify $< $@

tools/HermiT.jar:
	wget -O /tmp/HermiT.zip http://www.hermit-reasoner.com/download/current/HermiT.zip
	unzip /tmp/HermiT.zip -d tools

check.log: fff.xml tools/HermiT.jar
	java -jar tools/HermiT.jar -l $< -UcODP -o $@

.PHONY: clean
clean:
	rm -rf fff.ttl $(objects)
	rm -rf fff.owl fff.xml tools/{HermiT.jar,readme.txt,project,org.semanticweb.HermiT.jar}
	rm -rf check.log
