# Install

Setup a new virtualenv and install the dependencies in it:

    virtualenv -p python3 ve
    source ve/bin/activate
    pip install -r requirements.txt

Run:

    make
