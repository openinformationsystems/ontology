#!/usr/bin/env python3

from sys import argv
import ontospy

model = ontospy.Ontospy(argv[1])
model.printClassTree()
