#!/usr/bin/env python3

from sys import argv
from yaml import load
import yaml

from libs import definition

def stringify(inp):
    return '"' + inp + '"^^xsd:string'

source = argv[1]
source = load(open(source, 'r'))
print(yaml.dump(source))

dest = argv[2]
dest = open(dest, 'w')

classname = source['classname']
comment = source['comment']
label = source['label']

print("parsing class " + source['classname'])

classline = "fff:" + classname
classdef = definition(classline)

definitions = [classdef]

classdef.p('rdfs:isDefinedBy', 'fff:')
classdef.p('a', 'owl:Class')
classdef.p('rdfs:comment', stringify(comment))
classdef.p('rdfs:label', stringify(label))

try:
    clsses = source['sameClasses']
    for cls in clsses:
        classdef.p('owl:sameAs', cls)
except KeyError:
    pass

try:
    clsses = source['equivalentClasses']
    for cls in clsses:
        classdef.p('owl:equivalentClass', cls)
except KeyError:
    pass

try:
    properties = source['properties']
    for (prop_name, prop_properties) in properties.items():
        print("Property " + prop_name + " of class " + classname)
        propdef = definition('fff:' + prop_name)
        propdef.p('a', 'rdf:Property')
        propdef.p('rdfs:domain', classline)
        for (n, v) in prop_properties.items():
            propdef.p(n,v)
        definitions.append(propdef)
except KeyError:
    pass

for d in definitions:
    d.out(dest)
